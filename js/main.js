// Initialize fullPage.js
$(document).ready(function () {
  $('#js-fullpage').fullpage({
    //Navigation
    menu: '#menu',
    anchors: ['home', 'mission', 'whyus', 'contact'],
    navigation: true,
    navigationPosition: 'right',
    navigationTooltips: ['Početna', 'Misija', 'Karakteristike', 'Kontakt'],
    verticalCentered: false,
    responsiveWidth: 1100,

    //Accessibility
    keyboardScrolling: true,
    animateAnchor: true,
    keyboardScrolling: true,

  });
});